/* Functions for 

*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pcap.h>
#include <netinet/if_ether.h>
#include <sys/ioctl.h>        // macro ioctl is defined
#include <bits/ioctls.h>      // defines values for argument "request" of ioctl.
#include <net/if.h>           // struct ifreq
#include <unistd.h>           // close()
#include <netinet/ether.h>
#include <net/ethernet.h>
#include "iprouter.h"

// Global variables
pthread_t if_thread[NO_OF_INTF];
struct sniff_ethernet *mac_details;
struct sniff_ip *ip_details;

pcap_t *write_handle;

void my_callback(u_char *useless,const struct pcap_pkthdr* pkthdr,const u_char* packet)
{
	static int count = 1;

	fprintf(stdout,"\nPacket number [%d], length of this packet is: %d\n", count++, pkthdr->len);
	mac_details = (struct sniff_ethernet*)(packet);
	ip_details = (struct sniff_ip*)(packet + SIZE_ETHERNET);

	char *ip = (char *)inet_ntoa(ip_details->ip_src);
	//u_char *src_mac= (u_char *)ether_ntoa(mac_details->ether_shost); // this will be useful in figuring out the next hop in routing table
	//u_char *dest_mac=mac_details->ether_dhost; //this will give u the usRTR's MAC address , just a check
	printf("The ip address of the source i.e node3 in this case %s\n",ip);

	printf("The source mac address of the source i.e mac address of the rtr1 in this case :  ");
	print_mac_address(mac_details->ether_shost);
	
	//printf("The dest mac address of the source is %s\n",dest_mac);

	//call packet_parse() function and packet_forward() function
	packet_parser((unsigned char *) packet, pkthdr->len);

}

route_e *FindRouteEntry(char *dest_net_ip, char *subnet_mask)
{
	route_e *entry_found=NULL;
	ListElem *curElem=NULL,*firstElem=NULL;
	char *to_find_dest,*to_find_subnet;

	//inet_aton(dest_net_ip,&to_find_dest);
	//inet_aton(subnet_mask,&to_find_subnet);
	to_find_dest=malloc(sizeof(char)*IP_ADDR_LEN);
	to_find_subnet=malloc(sizeof(char)*IP_ADDR_LEN);

	firstElem=ListFirst(&route_t);
	for(curElem=firstElem;curElem!=NULL;curElem=ListNext(&route_t,curElem))
	{
		entry_found= (route_e *) curElem->obj;
		to_find_dest=inet_ntoa(entry_found->dest_addr);
		to_find_subnet=inet_ntoa(entry_found->netmask);

		if((strcmp(dest_net_ip,to_find_dest)==0) && (strcmp(subnet_mask,to_find_subnet)==0))
			break;
		else
			entry_found=NULL;
	}
	return entry_found;
}

int isRouteAlreadyAdded(char *dest_net_ip,char *subnet_mask)
{
	route_e *added=FindRouteEntry(dest_net_ip,subnet_mask);
	if (added == NULL)
		return 0;
	else
		return 1;
}

void remove_from_routing_table(char *dest_net_ip,char *subnet_mask)
{
	route_e *entry_to_remove = FindRouteEntry(dest_net_ip,subnet_mask);
	if (entry_to_remove != NULL)
	{
		ListElem *elem=ListFind(&route_t, (void *) entry_to_remove);
		if (elem != NULL)
		{
			ListUnlink(&route_t,elem);
			free(entry_to_remove);
		}
		else
			fprintf(stdout,"Inconsistency: List elem does not exist but entry in routing table exist\n");
	}
	else
		fprintf(stdout,"No entry found exist in the routing table\n");

	return;
}

void add_to_routing_table(char *dest_net_ip,char *subnet_mask,char *next_hop_ip,unsigned short int metric,char *eth_name)
{
	route_e *new_route=NULL;
	new_route=malloc(sizeof(route_e));
	inet_aton(dest_net_ip,&new_route->dest_addr);
	inet_aton(subnet_mask,&new_route->netmask);
	inet_aton(next_hop_ip,&new_route->next_hop);
	new_route->metric=metric;
	strcpy((char *)new_route->interface,eth_name);
	ListAppend(&route_t,new_route);

	return;
}

int validate_and_process(char *command)
{
	int i=0,result=1;
	//int array_count=0;
	char tokenArray[NO_Of_TOKENS][CMD_LEN];
	char *buffer=NULL;

	if(command != NULL)
	{
		bzero(tokenArray[i],sizeof(char)*CMD_LEN);
		strcpy(tokenArray[i],strtok(command," \n\r\t"));
		i++;
		while(command != NULL)
		{
			bzero(tokenArray[i],sizeof(char)*CMD_LEN);
			strcpy(tokenArray[i],strtok(command," \n\r\t"));
			i++;
		}
	}
	//array_count=i;
	free(buffer);

	if(strcasecmp(tokenArray[0],"help") == 0)
	{
		fprintf(stdout,"List of valid commands\n");
		fprintf(stdout,"add ip route **network_ip** **subnet_mask** **next_hop_ip**\n");
		fprintf(stdout,"no ip route **network_ip** **subnet_mask** **next_hop_ip**\n\n");
	}
	else if((strcasecmp(tokenArray[0],"add") == 0) && (strcasecmp(tokenArray[1],"ip") == 0) && (strcasecmp(tokenArray[2],"route") == 0))
	{
		if(isRouteAlreadyAdded(tokenArray[3],tokenArray[4]))
			fprintf(stdout,"Route already exists in Routing table\n");
		else
			add_to_routing_table(tokenArray[3],tokenArray[4],tokenArray[5],1,"eth1");
	}
	else if((strcasecmp(tokenArray[0],"no") == 0) && (strcasecmp(tokenArray[1],"ip") == 0) && (strcasecmp(tokenArray[2],"route") == 0))
	{
		if(!isRouteAlreadyAdded(tokenArray[3],tokenArray[4]))
			fprintf(stdout,"No route exists in Routing table\n");
		else
			remove_from_routing_table(tokenArray[3],tokenArray[4]);
	}
	else
	{
		fprintf(stdout,"Invalid command.\nTry \"help\" to see list of valid commands\n");
		result=0;
	}
	return result;
}

void *get_kbd_input(void *args)
{
	char *cmd=NULL;
	cmd=malloc(sizeof(char)*CMD_LEN);
	while (1)
	{
		bzero(cmd,sizeof(char)*CMD_LEN);
		fgets(cmd,sizeof(char)*CMD_LEN,stdin);
		if(strcasecmp(cmd,"exit") == 0)
			break;
		validate_and_process(cmd);
	}
	free(cmd);
	return NULL;
}

int main (int argc, char **argv)
{
	//Initialize
	initialize();
	
	// populate the list of interfaces
	populateInterfaces();

	// populate the list of neighboring nodes MAC addresses
	if (populateRemoteIfs() < 0) {
		fprintf(stderr, "Failed in populateRemoteIfs\n");
		return 0;
	}

	
	/* call the sniffing code */
	packetSniffer();
	
	//Call sendARP for gettig MAC addresses
	//sendARPRequest();
	
	//Join on the threads
	
	
	//Reset the conn state
	reset();
	
	return 0;
}

void packet_parser(unsigned char*pkt, unsigned len) {

	// First We make sure pkt is not a null pointer
	
	int size_ip;
	
	if ( NULL ==  pkt) { 
		fprintf(stderr,"Pkt pointer is NULL !! error quitting !!\n");
		exit(0);
	}
	
	/* define Ethernet header */ 
	struct sniff_ethernet *etherHdr;
	struct sniff_ip *ipHdr;
	
	/* Pointer to Ethernet Header */
	etherHdr=(struct sniff_ethernet*)pkt;
	
	/* Pointer to Ip header */ 
	ipHdr=(struct sniff_ip*) pkt+ SIZE_ETHERNET;
	
	size_ip = IP_HL(ipHdr)*4;
	if (size_ip < 20) {
		printf("   * Invalid IP header length: %u bytes\n", size_ip);
		exit(0);
	}
	
	/* Check if it is a ARP packet */ 
	if ( etherHdr->ether_type == ARP ) { 
		
#ifdef DEBUG 
		fprintf(stdout,"ARP packet found !! \n");
#endif
		/* Now Call Arp handler */ 
		ARPHandler(pkt);
	} else if (memcmp(etherHdr->ether_dhost,"01:00:5e",sizeof("01:00:5e"))) { 
#ifdef DEBUG 
		fprintf(stdout,"Got a Multicast Packet!! \n");
#endif	
		/* Do Nothing */		
	} else if ( etherHdr->ether_type == IP) { 
	
#ifdef DEBUG 
		fprintf(stdout,"IP packet:\n");
#endif

		/* Now identify if its ICMP */ 
		
		/* determine protocol */	
		switch(ipHdr->ip_p) {
			case IPPROTO_TCP:
#ifdef DEBUG 
		fprintf(stdout,"Protocol: TCP\n");
#endif	
			packet_forward(pkt, len);
			break;
			case IPPROTO_UDP:
#ifdef DEBUG 
		fprintf(stdout,"Protocol: UDP\n");
#endif	
			packet_forward(pkt, len);
			break;	
			case IPPROTO_ICMP:
#ifdef DEBUG 
		fprintf(stdout,"Protocol: ICMP\n");
#endif	
			ICMPHandler(pkt);
			break;
			case IPPROTO_IP:	
#ifdef DEBUG 
		fprintf(stdout,"Protocol: IP\n");
#endif	
			packet_forward(pkt, len);
			break;	
		} 
		
	} else if ( etherHdr->ether_type == RARP) {

#ifdef DEBUG 
		fprintf(stdout,"RARP packet found:\n");
#endif	
		/* Having a handler here for RARP */
		/* Do nothing here as of now */
	}
	
}

void packet_forward(unsigned char* pkt, unsigned len)
{
	struct sniff_ethernet *ether = NULL;
	struct sniff_ip *ip = NULL;
	
	if(pkt == NULL)
		DEBUG_ERR("passed packet is NULL");
	
	//fetch the Ethernet header
	ether = 	(struct sniff_ethernet *) pkt;
	
	//fetch the IP header
	ip = (struct sniff_ip *) ((unsigned char *) pkt + SIZE_ETHERNET);
	
	/* check if the packet needs to be forwarded */
	if(isForwardingRequired(pkt))
	{
		//Perform a route lookup
		route_e * r_entry = routeLookup(&ip->ip_dst);
		ASSERT(r_entry != NULL);
		
		//get Mac address
		if_e *arp_entry = arpLookup(&r_entry->dest_addr);
		ASSERT(arp_entry != NULL);
		
		//Change the TTL field
		ip->ip_ttl = ip->ip_ttl - 1;
		
		//recalculate checksum
		ip->ip_sum = 0;
		uint16_t ip_len = ip->ip_len;
		ip->ip_sum = checksum((uint16_t *)ip, ip_len);
		
		//Modify ethernet frame
		memcpy((unsigned char *) ether->ether_dhost, (unsigned char *) arp_entry->macaddr, ETHER_ADDR_LEN);
		
		/* print source and destination IP addresses */
		printf("       From: %s\n", inet_ntoa(ip->ip_src));
		printf("         To: %s\n", inet_ntoa(ip->ip_dst));
		printf("       SRC MAC:");
		print_mac_address(ether->ether_shost);
		printf("       DST MAC:");
		print_mac_address(ether->ether_dhost);
		
		// Write the Ethernet frame to the interface.
	    if (pcap_inject(write_handle,pkt,len)==-1) {
	        pcap_perror(write_handle,0);
	        pcap_close(write_handle);
	        exit(1);
	    }
	}
}

int isForwardingRequired (unsigned char *pkt ) { 

	struct sniff_ip *ipHdr;
	
	if ( NULL == pkt) {
		fprintf(stderr,"pkt is pointing to NULL !!\n");
		exit(0);
	}
	
	ipHdr = (struct sniff_ip*) (pkt + SIZE_ETHERNET);
	
	/* TTL check */
	
	if ( ipHdr->ip_ttl == 0 ) { 	
#ifdef DEBUG
		fprintf(stdout,"Pkt ttl was Zero , Sending Time Exceed Message \n");
#endif
		/* calling ICMP exceed message */ 
		icmpTimeExceed(pkt);
		return 0;	
	}
	
	/* checking if it is local */
	if(isAddressLocal(&ipHdr->ip_dst)){
#ifdef DEBUG
		fprintf(stdout,"Pkt was meant for local host, Dropping it !!\n");
#endif	
		return 0;
	}
	
	/* Broadcast addr or Multicast */ 
	if(isInvalid(&ipHdr->ip_dst)) { 
#ifdef DEBUG
		fprintf(stdout,"Broadcast or Multicast Pkt, Dropping it !!\n");
#endif	
		return 0;	
	
	}
	
	return 1;
}

int isInvalid (struct in_addr *dstAddr) { 

	char *dstBuf = inet_ntoa(*dstAddr); 
	char *dotPtr;
	int count=0;
	
	dotPtr=strtok(dstBuf,".");
	while( dotPtr != NULL) { 
		if (strncmp(dotPtr,"255",3) ) { 
			if ( count == 0 || count == 3) {
				return 1;
			}
		}
		count++;
		dotPtr=strtok(NULL,".");
	}
	return 0;
}

int isAddressLocal(struct in_addr *dstAddr) { 
	
	ListElem *elem=NULL;
	if_e *if_entry = NULL;
	
	for(elem=ListFirst(&myIntf);elem!=NULL;elem=ListNext(&myIntf,elem)) { 
		if_entry = (if_e *) elem->obj;		
		if( if_entry->ipaddr.s_addr == dstAddr->s_addr) { 
			return 1;
		}
	}
	return 0;
}

/*
	Does a longest prefix matching
	
*/
route_e *routeLookup(struct in_addr* destip)
{
	ListElem *elem = NULL;
	long ipaddr = ntohl(destip->s_addr);
	long netaddr, netmask;
	route_e * r_entry;
	long longestmask = 0;
	route_e *res = NULL;
	
	// Loop over route_t
	for(elem = ListFirst(&route_t); elem != NULL; elem = ListNext(&route_t, elem))
	{
		r_entry = (route_e *) elem->obj;
		
		netaddr = ntohl(r_entry->dest_addr.s_addr);
		netmask = ntohl(r_entry->netmask.s_addr);
		
		// mask dest_ip with net mask
		if((netmask & ipaddr) == netaddr)
		{
			if(longestmask < netmask)
			{	
				longestmask = netmask;
				res = r_entry;
				
			}
				
		}
	}
	
	
	return res;
}

if_e *ifLookup(uint8_t *ifname)
{
	if_e *res = NULL;
	
	// Loop over if_t
	
	// return the mac address
	
	return res;
}

if_e *arpLookup(struct in_addr* addr)
{
	if_e *res = NULL;
	
	// Loop over if_t
	
	// return the mac address
	
	return res;
}


void populateInterfaces () { 

	/* Assuming the global Variable  \
	   to list of interfaces to be defined */
	
	struct ifreq ifr;
	struct ifreq ifr1;

	char str_ipaddr[INET_ADDRSTRLEN];
	int sendsd, udpsock;
	if_e *tempElem;
	//struct sockaddr_in *dev_addr_in;
	
	
	pcap_if_t *alldevs, *dev; 
	char errbuf[PCAP_ERRBUF_SIZE];
	
	/*  creating the raw socket to query for the Interface info */
	if ((sendsd = socket (AF_INET, SOCK_RAW, htons(ETH_P_ALL)) ) < 0) {
    	perror ("socket() failed to get socket descriptor for using ioctl() ");
    	exit (0);
  	}

  	// to obtain the ip address
  	if ((udpsock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP)) < 0) {
  		perror ("socket() failed to get socket descriptor for using ioctl() ");
    	exit (0);	
  	}
	
	/* getting the list of interfaces */
	if (pcap_findalldevs(&alldevs,errbuf) == -1 ) {
		fprintf(stderr,"Error in pcap_findalldevs: %s\n", errbuf);
        exit(1);
    }
    
    for(dev=alldevs; dev != NULL; dev=dev->next) {
        
        /* Setting the memory here */
        tempElem = (if_e *)malloc(sizeof(if_e));
        
        if ( NULL == tempElem) { 
        	fprintf(stderr,"Malloc returned error when setting memoery for tempElem\n");
        	exit(0);
        }
        
        // clear out temp buffers
        memset (&ifr, 0, sizeof (ifr));	
	  	memset (&ifr1, 0, sizeof (ifr1));
	  		
	  	strcpy(ifr.ifr_name,dev->name);
	  	strcpy(ifr1.ifr_name,dev->name);

/*#ifdef DEBUG
	  	fprintf(stdout, "Device name: %s ifr.ifr_name: %s\n", dev->name, ifr.ifr_name);
#endif*/
	  	if (strcmp(ifr.ifr_name,"any" ) == 0 || strcmp(ifr.ifr_name,"lo" ) == 0 ) {
	  		
	  		continue;

	  		// re-enter for loop for next device
	  	} else {

	  		if (ioctl (udpsock, SIOCGIFADDR, &ifr) < 0) {
    			perror ("ioctl() failed to get source ip address ");
    			exit(0);
 	   		}
 	   
 	   		strcpy(str_ipaddr, inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
#ifdef DEBUG
	  		fprintf(stdout, "Call to ioctl was successful: %s\n", str_ipaddr);
#endif
 	   
 	   		if ( ( strncmp(str_ipaddr,"192.168", strlen("192.168") ) == 0) 
 	   		 	|| ( strncmp(str_ipaddr,"172.16", strlen("172.16") ) == 0 ) ) {

 	   		 	// don't do anything
 	   		} else {

		  		if (ioctl (sendsd, SIOCGIFHWADDR, &ifr1) < 0) {
	    			perror ("ioctl() failed to get source MAC address ");
	    			exit(0);
	  			}
  				
  				if (ifr1.ifr_hwaddr.sa_family!=ARPHRD_ETHER) {
				    fprintf(stderr, "Not and ethernet interface\n");
				    exit(0);
				}
  			
	  			memcpy(&tempElem->ipaddr, &((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr, sizeof(struct in_addr));
	  			
	  			memcpy(tempElem->macaddr, &ifr1.ifr_hwaddr.sa_data , 6);

	  			const unsigned char* mac= tempElem->macaddr;
				
	  			
	  			strcpy(tempElem->inf, dev->name);

	  			/* List append Here- ADD Code HERE */ 
		    	ListAppend(&myIntf, (void*) tempElem);
  			
#ifdef DEBUG
/*   			ListElem *elem = NULL;
				for(elem=ListFirst(&myIntf); elem!=NULL; elem = ListNext(&myIntf, elem))
				{
					if(elem->obj == tempElem)
					{
						if_e * pIF = (if_e *)elem->obj;
						mac= pIF->macaddr;
						fprintf(stdout, "Device-Name: %s, Ip-Add : %s, Mac-address: ",pIF->inf,inet_ntoa(pIF->ipaddr));
						fprintf(stdout, "%02X:%02X:%02X:%02X:%02X:%02X\n", mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
					}
				}
*/
    			fprintf(stdout, "Device-Name: %s, Ip-Add : %s, Mac-address: ",tempElem->inf,inet_ntoa(tempElem->ipaddr));
    			fprintf(stdout, "%02X:%02X:%02X:%02X:%02X:%02X\n", mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
    			fprintf(stdout, "Current list length after appending: %d\n", ListLength(&myIntf));
#endif 	
	  		}
	   
	   		
		    
		}

		//free(tempElem);
	}

    close(sendsd);
    close(udpsock);
    
}

/* Sniffs packets */
void packetSniffer()
{
	ListElem *firstElem=NULL,*curElem=NULL;
	if_e *if_details;
	int i=0;

	firstElem=ListFirst(&myIntf);
	for(curElem=firstElem;curElem!=NULL;curElem=ListNext(&myIntf,curElem))
	{
		if_details=curElem->obj;
		fprintf(stdout,"The interface name is %s\n",if_details->inf);
		pthread_create(&if_thread[i],NULL,(void *)&thrPacketBlock,(void *)if_details->inf);
		i++;
	}

	return;
}


/* thread block for packet sniffer */
void * thrPacketBlock (void *arg)
{
	char* dev = (char *) arg;
	pcap_t *handle;			/* Session handle */
	//char *dev,*new_dev;		/* The device to sniff on */
	char errbuf[PCAP_ERRBUF_SIZE];	/* Error string */
	//struct bpf_program fp;		/* The compiled filter */
	//char filter_exp[] = "src 10.1.0.3";	/* The filter expression to only capture the packets coming from node3 */
	//bpf_u_int32 mask;		/* Our netmask */
	//bpf_u_int32 net;		/* Our IP */

	// open a pcap live session on passed interface
	/* Open the session in promiscuous mode */
	handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
	if (handle == NULL)
	{
		fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
		exit(0);
	}

	//call pcap_loop()
	while(pcap_loop(handle,-1, my_callback, NULL) != -1);

	// the functions mentioned below will be called in the callback
	//function


	//get packet and call packet_parse

	//packet_parser();

	//after parsing, route the packet after referring to the
	//routing table

	//packet_forwarding();
	pcap_close(handle);
	return NULL;
}

/* send ARP messages and get */
void sendARPRequest()
{

#if 0
	ListElem *elem = NULL;
	struct if_e * arp_entry;
	
	// Loop over route_t
	for(elem = ListFirst(&arp_t); elem != NULL; elem = ListNext(&arp_t, elem))
	{
		arp_entry = (struct if_e *) elem->obj;
		
		// Construct Ethernet header (except for source MAC address).
	    // (Destination set to broadcast address, FF:FF:FF:FF:FF:FF.)
	    struct ether_header header;
	    header.ether_type=htons(ETH_P_ARP);
	    memset(header.ether_dhost,0xff,sizeof(header.ether_dhost));
	
	    // Construct ARP request (except for MAC and IP addresses).
	    struct ether_arp req;
	    req.arp_hrd=htons(ARPHRD_ETHER);
	    req.arp_pro=htons(ETH_P_IP);
	    req.arp_hln=ETHER_ADDR_LEN;
	    req.arp_pln=sizeof(in_addr_t);
	    req.arp_op=htons(ARPOP_REQUEST);
	    memset(&req.arp_tha,0,sizeof(req.arp_tha));
	
	    
	    memcpy(&req.arp_tpa,&arp_entry->ipaddr.s_addr,sizeof(req.arp_tpa));
	
	    // Write the interface name to an ifreq structure,
	    // for obtaining the source MAC and IP addresses.
	    struct ifreq ifr;
	    size_t if_name_len=strlen(if_name);
	    if (if_name_len<sizeof(ifr.ifr_name)) {
	        memcpy(ifr.ifr_name,if_name,if_name_len);
	        ifr.ifr_name[if_name_len]=0;
	    } else {
	        fprintf(stderr,"interface name is too long");
	        exit(1);
	    }
	
	    // Open an IPv4-family socket for use when calling ioctl.
	    int fd=socket(AF_INET,SOCK_DGRAM,0);
	    if (fd==-1) {
	        perror(0);
	        exit(1);
	    }
	
	    // Obtain the source IP address, copy into ARP request
	    if (ioctl(fd,SIOCGIFADDR,&ifr)==-1) {
	        perror(0);
	        close(fd);
	        exit(1);
	    }
	    struct sockaddr_in* source_ip_addr = (struct sockaddr_in*)&ifr.ifr_addr;
	    memcpy(&req.arp_spa,&source_ip_addr->sin_addr.s_addr,sizeof(req.arp_spa));
	
	    // Obtain the source MAC address, copy into Ethernet header and ARP request.
	    if (ioctl(fd,SIOCGIFHWADDR,&ifr)==-1) {
	        perror(0);
	        close(fd);
	        exit(1);
	    }
	    if (ifr.ifr_hwaddr.sa_family!=ARPHRD_ETHER) {
	        fprintf(stderr,"not an Ethernet interface");
	        close(fd);
	        exit(1);
	    }
	    const unsigned char* source_mac_addr=(unsigned char*)ifr.ifr_hwaddr.sa_data;
	    memcpy(header.ether_shost,source_mac_addr,sizeof(header.ether_shost));
	    memcpy(&req.arp_sha,source_mac_addr,sizeof(req.arp_sha));
	    close(fd);
	
	    // Combine the Ethernet header and ARP request into a contiguous block.
	    unsigned char frame[sizeof(struct ether_header)+sizeof(struct ether_arp)];
	    memcpy(frame,&header,sizeof(struct ether_header));
	    memcpy(frame+sizeof(struct ether_header),&req,sizeof(struct ether_arp));
	
	    // Open a PCAP packet capture descriptor for the specified interface.
	    char pcap_errbuf[PCAP_ERRBUF_SIZE];
	    pcap_errbuf[0]='\0';
	    pcap_t* pcap=pcap_open_live(if_name,96,0,0,pcap_errbuf);
	    if (pcap_errbuf[0]!='\0') {
	        fprintf(stderr,"%s\n",pcap_errbuf);
	    }
	    if (!pcap) {
	        exit(1);
	    }
	
	    // Write the Ethernet frame to the interface.
	    if (pcap_inject(pcap,frame,sizeof(frame))==-1) {
	        pcap_perror(pcap,0);
	        pcap_close(pcap);
	        exit(1);
	    }

	}
#endif
	
}

int populateRemoteIfs()
{
	int num, type, flags;
	FILE *fp = fopen( "/proc/net/arp", "r" );
	char ip[128]   = { 0x00 };
	char hwa[128]  = { 0x00 };
	char mask[128] = { 0x00 };
	char line[128] = { 0x00 };
	char dev[128]  = { 0x00 };

#ifdef DEBUG
	fprintf(stdout, "Entered populateRemoteIfs\n");
#endif
	struct ether_addr ethaddr;
	memset(&ethaddr, 0, sizeof(struct ether_addr));

	if_e *p_if; 

	if( !fp ) {
		fprintf(stderr, "Unable to open /proc/net/arp for reading\n");
		return -1;
	}

	fgets( line, sizeof(line), fp ); // skip  the first line

	while( fgets( line, sizeof(line), fp ) )
	{
		num = sscanf( line, "%s 0x%x 0x%x %s %s %s\n", ip, &type, &flags, hwa, mask, dev );
		
		if( num < 4 ) {
			fprintf(stderr, "Broken line while parsing arp file\n");
			return -1;
		}
		
		if ( (strncmp( ip, "192.168", strlen("192.168") ) == 0 )
				|| (strncmp( ip, "172.16", strlen("172.16") ) == 0 ) ) {
			// do nothing
		} else {
			
			p_if = (if_e *) malloc (sizeof(if_e));

			if (inet_aton(ip, &p_if->ipaddr) < 0) {
				fprintf(stderr, "Problem converting the read ip address:%s\n", ip);
				return -1;
			}
			if (ether_aton_r( hwa, &ethaddr) == NULL) {
				fprintf(stderr, "Problem converting the read mac address:%s\n", ip);
				return -1;
			}

			int i = 0;
			for (;i < 6; ++i) {
				p_if->macaddr[i] = ethaddr.ether_addr_octet[i];
			}

			strncpy(p_if->inf, dev, INTERFACE_LEN);

			const unsigned char* mac= p_if->macaddr;

			ListAppend(&remoteIntf, p_if);

#ifdef DEBUG
			fprintf(stdout, "Device-Name: %s, Ip-Add : %s, Mac-address: ",p_if->inf,inet_ntoa(p_if->ipaddr));
			fprintf(stdout, "%02X:%02X:%02X:%02X:%02X:%02X\n", mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
#endif

		}
	}

	fclose( fp );

	return 0;
}


/* Do we require it ??
*/
void sendARPReply()
{
}


void ARPHandler(unsigned char *arp_pkt)
{
	// Check if Request or Reply
	
	// If Request, do we need to send the reply?
	
	// If reply, fetch the MAC address
	
	//update arp_entry 
}


/*
	What needs to be done
*/	
void ICMPHandler(uint8_t *arp_pkt)
{
	// Check if Request or Reply
	 
}

/* Initialize the list variables */
void initialize()
{
	//open sozkets for writing
	
	//Initialize lists

	ListInit(&myIntf);
	ListInit(&route_t);
	ListInit(&remoteIntf);

}

/* Reset the variables */

void reset()
{
	//Make the lists empty
	
	//remove connection state
	
	//close any sockets
}

// Checksum function
uint16_t
checksum (uint16_t *addr, uint16_t len)
{
  int nleft = len;
  int sum = 0;
  uint16_t *w = addr;
  uint16_t answer = 0;

  while (nleft > 1) {
    sum += *w++;
    nleft -= sizeof (uint16_t);
  }

  if (nleft == 1) {
    *(uint8_t *) (&answer) = *(uint8_t *) w;
    sum += answer;
  }

  sum = (sum >> 16) + (sum & 0xFFFF);
  sum += (sum >> 16);
  answer = ~sum;
  return (answer);
}

void print_mac_address(const unsigned char *addr){
	
	int i =0;
	for(;i<ETHER_ADDR_LEN;i++)
		if(i<ETHER_ADDR_LEN-1)
			printf("%02x::", *(addr +i));
		else
			printf("%02x\n", *(addr +i));	
}

void icmpTimeExceed(unsigned char* tempPkt) { 

#if 0
	struct sniff_ethernet *etherHdr,*myEtherHdr; 
	struct sniff_ip *ipHdr,*myIpHdr; 
	struct icmphdr *icmpHdr;
	
	etherHdr= (struct sniff_ethernet*)tempPkt;
	ipHdr=(struct sniff_ip*) tempPkt+SIZE_ETHERNET;
	
	
    /* making Icmp Packet */ 
    unsigned char* pkt=NULL;
    pkt= (char*) malloc(sizeof(struct sniff_ethernet)+sizeof(sniff_ip)+sizeof(struct icmphdr)+8);
    
    if ( NULL == pkt ) { 
    	fprintf(stderr,"Malloc returned error !!\n");
    	exit(0);
    }	
    memset(pkt,0,sizeof(struct sniff_ethernet)+sizeof(sniff_ip)+sizeof(struct icmphdr)+8);
    
    myEtherHdr=(struct sniff_ethernet*)pkt;
    
    /* Making Ethernet Header */ 
    myEtherHdr->ether_dhost=etherHdr->ether_shost;
    myEtherHdr->ether_shost=etherHdr->ether_dhost;
    myEtherHdr->ether_type=etherHdr->ether_type;
    
    /*Making Ip header */ 
    myIpHdr=(struct sniff_ethernet*)pkt+SIZE_ETHERNET;
    
    myIpHdr->ip_vhl=ipHdr->ip_vhl;
    myIpHdr->ip_tos=ipHdr->ip_tos;
    myIpHdr->ip_len=sizeof(sniff_ip)+sizeof(struct icmphdr)+8;
    myIpHdr->ip_id=ipHdr->ip_id;
    myIpHdr->ip_off=ipHdr->ip_off;
    myIpHdr->ip_ttl=64;   /* setting TTL value of 64 */
    myIpHdr->ip_p=IPPROTO_ICMP   /* Setting to ICMP */
    /* TODO :Need to Figure out away to put my Address */
    /* Note they are struct of type in_addr , you can't do a assignment here */
    myIpHdr->ip_src= ;
	strcpy(myIpHdr->ip_src,ipHdr->ip_dst);   
	/* TODO : Checksum taken from Internet, We need to see what needs to be done */
	myIpHdr->ip_sum=checksum(uint16_t* myIpHdr,20);
	
	/* Now setting the ICMP header */
	icmpHdr= (struct icmphdr*)pkt+SIZE_ETHERNET+sizeof(sniff_ip);
	
	icmpHdr->type=ICMP_TIME_EXCEEDED;
	icmpHdr->code=0;
	
	/* TODO: Get the function from Internet or Write ?? */
	/* TODO : Continue till you done with payload and send packet out */
	icmpHdr->checksum=  ;

#endif
	
}

