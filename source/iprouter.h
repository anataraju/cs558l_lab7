/*
	Header file for IP router functionality
	
*/

#include <netinet/in.h>
#include "list.h"

#define ASSERT(x) if(!(x)) { fprintf( stderr,  "%s (%d): Assertion failed!!!\n", __FILE__, __LINE__); }
#define IS_SET(s, b) ((s & b) == b)

#define DEBUG

#define DEBUG_ERR(msg) {fprintf(stderr, "%s (%d): %s\n", __FILE__, __LINE__, msg);  exit(1);}
#ifdef DEBUG
	#define DEBUG_MSG(msg) {fprintf(stderr, "%s (%d): %s\n", __FILE__, __LINE__, msg);}
#else
	#define DEBUG_MSG(msg) {}//do nothing)
#endif 


#define INTERFACE_LEN 1024

/* default snap length (maximum bytes per packet to capture) */
#define SNAP_LEN 1518

/* ethernet headers are always exactly 14 bytes [1] */
#define SIZE_ETHERNET 14

#ifndef ETHER_ADDR_LEN
/* Ethernet addresses are 6 bytes */
#define ETHER_ADDR_LEN	6
#endif

#define CMD_LEN 1024
#define NO_Of_TOKENS 100
#define IP_ADDR_LEN 20
#define NO_OF_INTF 2

#define ARP 0x0806
#define IP 0x0800
#define RARP 0x8035

/* Ethernet header */
struct sniff_ethernet {
        unsigned char  ether_dhost[ETHER_ADDR_LEN];    /* destination host address */
        unsigned char  ether_shost[ETHER_ADDR_LEN];    /* source host address */
        unsigned short ether_type;                     /* IP? ARP? RARP? etc */
};

/* IP header */
struct sniff_ip {
        unsigned char  ip_vhl;                 /* version << 4 | header length >> 2 */
        unsigned char  ip_tos;                 /* type of service */
        unsigned short ip_len;                 /* total length */
        unsigned short ip_id;                  /* identification */
        unsigned short ip_off;                 /* fragment offset field */
        #define IP_RF 0x8000            /* reserved fragment flag */
        #define IP_DF 0x4000            /* dont fragment flag */
        #define IP_MF 0x2000            /* more fragments flag */
        #define IP_OFFMASK 0x1fff       /* mask for fragmenting bits */
        unsigned char  ip_ttl;                 /* time to live */
        unsigned char  ip_p;                   /* protocol */
        unsigned short ip_sum;                 /* checksum */
        struct  in_addr ip_src,ip_dst;  /* source and dest address */
};
#define IP_HL(ip)               (((ip)->ip_vhl) & 0x0f)
#define IP_V(ip)                (((ip)->ip_vhl) >> 4)

/* TCP header */
typedef u_int tcp_seq;

struct sniff_tcp {
        unsigned short th_sport;               /* source port */
        unsigned short th_dport;               /* destination port */
        tcp_seq th_seq;                 /* sequence number */
        tcp_seq th_ack;                 /* acknowledgement number */
        unsigned char  th_offx2;               /* data offset, rsvd */
#define TH_OFF(th)      (((th)->th_offx2 & 0xf0) >> 4)
        unsigned char  th_flags;
        #define TH_FIN  0x01
        #define TH_SYN  0x02
        #define TH_RST  0x04
        #define TH_PUSH 0x08
        #define TH_ACK  0x10
        #define TH_URG  0x20
        #define TH_ECE  0x40
        #define TH_CWR  0x80
        #define TH_FLAGS        (TH_FIN|TH_SYN|TH_RST|TH_ACK|TH_URG|TH_ECE|TH_CWR)
        unsigned short th_win;                 /* window */
        unsigned short th_sum;                 /* checksum */
        unsigned short th_urp;                 /* urgent pointer */
};

typedef struct session_state
{
	//What all session state is required
	//opened descriptors like sockets
	
	//Lists?
	
	unsigned conn_state;
}
session_t;

typedef struct routing_entry
{
	struct in_addr dest_addr;
	struct in_addr next_hop;
	struct in_addr netmask;
	unsigned metric;
	char interface[INTERFACE_LEN];
} route_e;	

/* contain the neighbour information.
Populate initially with IP addresses of neigbours,
Mac addresses and interfaces we will populate later 
depending on ARP reply */

typedef struct interface_entry
{
	char inf[INTERFACE_LEN];
	unsigned char macaddr[6];
	struct in_addr ipaddr;
} if_e;

/* 
	create Lists or arrays of these entries
	
*/
List route_t;
List myIntf;
List remoteIntf;


/* Functions for 

*/


/* Manoj: */
void packet_parser(unsigned char* pkt, unsigned len);


/* MAnoj: */
void populateInterfaces();

/* Amogh */
int populateRemoteIfs();

/* Manoj */
int isForwardingRequired(unsigned char* pkt);

/* Manoj */
int isAddressLocal(struct in_addr *dstAddr);

/* Manoj */
/* */
route_e *routeLookup(struct in_addr* destip);

/* Manoj */
/* */
if_e *ifLookup(unsigned char *inf);

if_e *arpLookup(struct in_addr* addr);

/* Venkata: Sniffs packets */
void packetSniffer();

/* Venkata: thread block for packet sniffer */
void * thrPacketBlock (void *arg);

/* Venkata: poulate initial routes */
//void populateRoutingTable();

/* send ARP messages and get */
void sendARPRequest();

/* Do we require it ??
*/
void sendARPReply();

/* Ankit  */
void packet_forward(unsigned char* pkt, unsigned len);


/* Ankit */
void ARPHandler(unsigned char *arp_pkt);


/*
	What needs to be done
*/	
void ICMPHandler(unsigned char *arp_pkt);

/* Initialize the list variables */
void initialize();

/* Reset the variables */

void reset();

void print_mac_address(const unsigned char *addr);

uint16_t checksum (uint16_t *addr, uint16_t len);

int isInvalid (struct in_addr *dstAddr);

void *get_kbd_input(void *args);

int validate_and_process(char *command);
void add_to_routing_table(char *dest_net_ip,char *subnet_mask,char *next_hop_ip,unsigned short int metric,char *eth_name);
int isRouteAlreadyAdded(char *dest_net_ip,char *subnet_mask);
void remove_from_routing_table(char *dest_net_ip,char *subnet_mask);
route_e *FindRouteEntry(char *dest_net_ip, char *subnet_mask);
void my_callback(u_char *useless,const struct pcap_pkthdr* pkthdr,const u_char* packet);

void icmpTimeExceed(unsigned char* tempPkt);
	
	




